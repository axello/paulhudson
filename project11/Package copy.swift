// swift-tools-version:4.0
import PackageDescription

let package = Package(
    name: "project11",
    dependencies: [
        // 💧 A server-side Swift web framework.
        .package(url: "https://github.com/vapor/vapor.git", from: "3.0.8"),
        .package(url: "https://github.com/vapor/mysql-driver.git", from: "3.0.2"),
        .package(url: "https://github.com/vapor/leaf.git", from: "3.0.1"),
        .package(url: "https://github.com/crossroadlabs/Markdown.git", .exact("1.0.0-alpha.2")),
        .package(url: "https://github.com/twostraws/SwiftSlug.git", .upToNextMinor(from: "0.2.0")),
  ],
  targets: [
      .target(
          name: "App",
          dependencies: ["Vapor", "Leaf", "FluentMySQL", "Markdown", "SwiftSlug"]
      ),
      .target(name: "Run", dependencies: ["App"]),
      .testTarget(name: "AppTests", dependencies: ["App"]),
  ]
)

