//
//  Post.swift
//  App
//
//  Created by Axel Roest on 30/04/2018.
//

import Foundation
import FluentMySQL
import Vapor

struct Post: Content, MySQLModel {
    var id: Int?
    var title: String
    var strap: String
    var content: String
    var category: Int
    var slug: String
    var date: Date
}

extension Post: Migration {
//    static let entity: String = "Post"
    
    static func prepare(on connection: MySQLConnection) -> Future<Void> {
        return MySQLDatabase.create(self, on: connection) { builder in
            builder.field(for: \.id, isIdentifier: true) // , type: .int64()
            builder.field(for: \.title)
            builder.field(for: \.strap)
            builder.field(for: \.content, type: .text())
            builder.field(for: \.category)
            builder.field(for: \.slug)
            builder.field(for: \.date)
        }
    }
}
