Changes from swift book
Between Vapor 3.0.0 and 3.3.3

As `vapor update` is really slow, always use
`swift package --enable-pubgrub-resolver update`


2020 February

.configure
TemplateRenderer --> ViewRenderer
DatabaseConfig --> DatabasesConfig

Routes
req.parameter(String.self) --> req.parameters.next(String.self)
remove try before all Content.query()

Databases
for some strange reason, the sqlite database, which seems 99% equal, did not work. Had to create a new one
Aha!
In fluent 3.0.0-rc.2, when you have a Content class 'Forum', it created a table called 'forums'. Not anymore. The new & shiny fluent 3.0.0 creates a table called 'Forum'.
It is nicer, in that it is exactly the same name. But: WHY THIS BREAKING CHANGE???

Content
make(for parameter, using container) --> resolveParameter(_ parameter, on container)
    Futures are handled more simple


---
Problems with project5, 
SwiftGD 2.5.0 could not find gdImagePtr
brew reinstall gd
remove .build
clean project etc.
Double-check double setting of /usr/local/lib
downgraded to SwiftGD 2.4.0
In the end *removing* the xcodeproj was the only thing which worked
    
However, when I upgraded to SwiftGD 2.5.0 again, removing the xcodeproj and deep cleaning the build folder did not work anymore.
Saved build logs
downgraded to SwiftGD 2.4.0
