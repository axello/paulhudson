import Vapor
import Fluent
import FluentMySQL
import Leaf

/// Called before your application initializes.
///
/// [Learn More →](https://docs.vapor.codes/3.0/getting-started/structure/#configureswift)
public func configure(
    _ config: inout Config,
    _ env: inout Environment,
    _ services: inout Services
) throws {
    // Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)

    // Configure the rest of your application here
    try services.register(FluentMySQLProvider())
    let databaseConfig = MySQLDatabaseConfig(hostname: "192.168.8.130", port: 3306, username: "swift", password: "Swift123!", database: "swift", transport: .unverifiedTLS)
    services.register(databaseConfig)
    
    var migrationConfig = MigrationConfig()
    migrationConfig.add(model: Category.self, database: .mysql)
    migrationConfig.add(model: Post.self, database: .mysql)
    services.register(migrationConfig)
 
    // LEAF
    try services.register(LeafProvider())
    config.prefer(LeafRenderer.self, for: ViewRenderer.self)
    
    // TAGS
    var tags = LeafTagConfig.default()
    tags.use(MarkdownTag(), as: "markdown")
    tags.use(LinkTag(), as: "link")
    services.register(tags)
}
