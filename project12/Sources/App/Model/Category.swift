//
//  Category.swift
//  App
//
//  Created by Axel Roest on 30/04/2018.
//

import Foundation
import FluentMySQL
import Vapor

struct Category: Content, MySQLModel, Migration {
    var id: Int?
    var name: String
}
