// swift-tools-version:5.0
import PackageDescription

let package = Package(
    name: "project12",
    dependencies: [
        // 💧 A server-side Swift web framework.
        .package(url: "https://github.com/vapor/vapor.git", from: "3.3.3"),
        .package(url: "https://github.com/vapor/fluent-mysql-driver.git", from: "3.1.0"),
        .package(url: "https://github.com/vapor/leaf.git", from: "3.0.2"),
        .package(url: "https://github.com/twostraws/Markdown.git", from:"1.0.0"),
        .package(url: "https://github.com/twostraws/SwiftSlug.git", .upToNextMinor(from: "0.3.0")),
  ],
  targets: [
      .target(
          name: "App",
          dependencies: ["Vapor", "Leaf", "FluentMySQL", "Markdown", "SwiftSlug"]
      ),
      .target(name: "Run", dependencies: ["App"]),
      .testTarget(name: "AppTests", dependencies: ["App"]),
  ]
)

