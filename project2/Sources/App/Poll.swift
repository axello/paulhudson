//
//  Poll.swift
//  App
//
//  Created by Axel Roest on 09/04/2018.
//

import Foundation
import Vapor
import Fluent
import FluentSQLite



struct Poll : Content, SQLiteUUIDModel, Migration {
    static let name = "polls"
    
    var id: UUID?
    var title: String
    var option1: String
    var option2: String
    var votes1: Int
    var votes2: Int
}

