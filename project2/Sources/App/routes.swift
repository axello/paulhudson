import Foundation
import Routing
import Vapor
import Fluent
import FluentSQLite

/**
 Tests
 curl http://localhost:8080/polls/list
 curl -X POST http://localhost:8080/polls/create -d "title=What's the best color?" -d "option1=Jet Black" -d "option2=Rose Gold" -d "votes1=0" -d "votes2=0"

 */
/// Register your application's routes here.
///
/// [Learn More →](https://docs.vapor.codes/3.0/getting-started/structure/#routesswift)
public func routes(_ router: Router) throws {
    router.post(Poll.self, at: "polls", "create") { req, poll -> Future<Poll> in
        return poll.save(on: req)
    }

    router.get("polls", "list") { req -> Future<[Poll]> in
        return Poll.query(on: req).all()
    }

    router.get("polls", UUID.parameter) { req -> Future<Poll> in
        let id = try req.parameters.next(UUID.self)
        let poll = try Poll.find(id, on: req).map(to: Poll.self) { poll in
            guard let poll = poll else {
                throw Abort(.notFound)
            }
            return poll
        }
        return poll
    }

    router.delete("polls", UUID.parameter) { req -> Future<Poll> in
        let id = try req.parameters.next(UUID.self)
        let poll = try Poll.find(id, on: req).map(to: Poll.self) { poll in
            guard let poll = poll else {
                throw Abort(.notFound)
            }
            return poll
        }
        poll.delete(on: req)

        return poll
    }

    router.post("polls", "vote", UUID.parameter, Int.parameter) { req -> Future<Poll> in
        let id = try req.parameters.next(UUID.self)
        let vote = try req.parameters.next(Int.self)
        return try Poll.find(id, on: req).flatMap(to: Poll.self) { poll in
            guard var poll = poll else {
                throw Abort(.notFound)
            }
            if vote == 1 {
                poll.votes1 += 1
            } else {
                poll.votes2 += 1
            }
            return poll.save(on: req)
        }
    }
}
