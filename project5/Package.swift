// swift-tools-version:5.0
import PackageDescription

let package = Package(
    name: "project5",
    dependencies: [
        // 💧 A server-side Swift web framework.
        .package(url: "https://github.com/vapor/vapor.git", .upToNextMinor(from: "3.3.3")),
        .package(url: "https://github.com/vapor/leaf.git", .upToNextMinor(from: "3.0.2")),
        .package(url: "https://github.com/twostraws/SwiftGD.git", .upToNextMinor(from: "2.4.0")),
        .package(url: "https://github.com/vapor/multipart.git", from: "3.1.1"),
        ],
    targets: [
        .target(name: "App", dependencies: ["Vapor", "Leaf", "SwiftGD", "Multipart"]),
        .target(name: "Run", dependencies: ["App"]),
        .testTarget(name: "AppTests", dependencies: ["App"]),
    ]
)
