# Trials MultipartParser

Hi Tanner,
First of all, thanks for all the hard work you put into Vapor. I've been using it since version 1.5, but as web development is not my day job, it is hard to keep up with all the changes.

I am learning the new Vapor 3 with Paul Hudson's book, and noticed a lot of changes with the new Vapor 3 release API already. 
For chapter 5 I need to upload files. As I need that also for my application, it was worthwhile to pursue this task.
However, MultipartForm, which Paul used in the book is now obsolete.
I have had a long discussion last weekend on Slack, and even 0xtim could not help me, despite that he wrote a book on Vapor 3.

I guess the answer is trivially simple, but before you dismiss me with a 'did you look at the documentation' again, here is what I did.

## MultipartForm
Is now obsolete, deprecated and gone from the documentation, without a referral to its replacement, because it is probably trivial. Nothing can be found on SO, apart from old Vapor 2 and one obsolete Vapor 3 answer.

## Naive, File
Naively, as everything is now done using the Decodable protocol, one would think that you could decode directly to a File, which is decodable.
```
    router.post("upload2") { req -> Future<Response> in
        return try req.content.decode(File.self).map(to: Response.self, { form in
            let fn = form.filename
            print(fn)
            let newURL = originalsDirectory.appendingPathComponent("wtf_F_\(fn).png")
            print(newURL)
            //            print(user.name)
            //                print(file) // Raw image data
            
            _ = try? form.data.write(to: newURL)
            return req.redirect(to: "/", type: RedirectType.normal)          //.redirect(to: "/", [:])
        })
    }
```
However, then you get the response error:
```
{
error: true,
reason: "No multipart part named 'filename' was found."
}
```

## Naive, Files
Stupid me! I upload several files, so let's try that: change File.self to [File].self
```
    router.post("upload2") { req -> Future<Response> in
        return try req.content.decode([File].self).map(to: Response.self, { forms in
            for file in forms {
                let fn = file.filename
                print(fn)
                let newURL = originalsDirectory.appendingPathComponent("wtf_FS_\(fn).png")
                print(newURL)
                //            print(user.name)
                //                print(file) // Raw image data
                
                _ = try? file.data.write(to: newURL)
            }
            return req.redirect(to: "/", type: RedirectType.normal)          //.redirect(to: "/", [:])
        })
    }

```
However, as soon as I upload the files, I get the error:
```
{
error: true,
reason: "Nested form-data decoding is not supported."
}
```
Nested? There is nothing nested. I think.
I am using the example from the book:
```
    <form method="post" action="/upload2" enctype="multipart/form-data">
    <p><input type="file" name="upload" multiple/></p>
    <p><button type="submit" class="btn">Upload</button></p>
    </form>
```

## File in struct
As the book and other examples use a struct for all uploads, I started with that, in the following function. This is also documented in the documentation. 
This works for a single file upload. But when I try to upload multiple files, only one file is written. But no error is given.

```
    router.post("upload2") { req -> Future<Response> in
        struct ImageFile : Content {
            var upload: File
        }
        return try req.content.decode(ImageFile.self).map(to: Response.self, { form in
            let fn = form.upload.filename
            print(fn)
            let newURL = originalsDirectory.appendingPathComponent("wtf_IF-F_\(fn).png")
            print(newURL)
            //            print(user.name)
            //                print(file) // Raw image data
            
            _ = try? form.upload.data.write(to: newURL)
            return req.redirect(to: "/", type: RedirectType.normal)          //.redirect(to: "/", [:])
        })
    }
```

## Files in struct
Aha, but I need to upload multiple files, so I just create an array for upload!

```
    router.post("upload2") { req -> Future<Response> in
        struct ImageFile : Content {
            var upload: [File]
        }
        return try req.content.decode(ImageFile.self).map(to: Response.self, { forms in
            guard forms.upload.count > 0 else {
                return req.redirect(to: "/", type: RedirectType.normal)
            }
            for file in forms.upload {
//                print(filename)
                let newURL = originalsDirectory.appendingPathComponent("wtf_\(file.filename).png")
                print(newURL)
    //            print(user.name)
    //                print(file) // Raw image data

                _ = try? file.data.write(to: newURL)
            }
            return req.redirect(to: "/", type: RedirectType.normal)          //.redirect(to: "/", [:])
       })
    }
```
This does not work on several levels:
* for some reason, in all the examples, 'forms' is unknown to the debugger. It states it is of type 'ImageFile', but lldb can't do anything with it:
```
Server starting on http://localhost:8080(lldb) po forms
error: <EXPR>:3:1: error: use of unresolved identifier 'forms'
forms
^~~~~
```

The variable pane is also empty for forms:
[image:5B05C144-A237-4F59-B940-0B4C9E5F232C-905-00000C23F5F71485/Screen Shot 2018-04-17 at 11.54.24.png]
* when I remove the `guard forms.upload.count`, it doesn't matter: there is no upload count, so the `for file` also ends immediately

## File in struct array
Desperately, let's try to decode to an array of the decodable struct.
```
    router.post("upload2") { req -> Future<Response> in
        struct ImageFile : Content {
            var upload: File
        }
        return try req.content.decode([ImageFile].self).map(to: Response.self, { forms in
            guard forms.count > 0 else {
                return req.redirect(to: "/", type: RedirectType.normal)
            }
            for ifile in forms {
                let file = ifile.upload
                //                print(filename)
                let newURL = originalsDirectory.appendingPathComponent("wtf_\(file.filename).png")
                print(newURL)
                //            print(user.name)
                //                print(file) // Raw image data
                
                _ = try? file.data.write(to: newURL)
            }
            return req.redirect(to: "/", type: RedirectType.normal)          //.redirect(to: "/", [:])
        })
    }
```

But that gives the familiar error again:
```
{
error: true,
reason: "Nested form-data decoding is not supported."
}
```

## Back to basics,
The new code for Multipart came out before the weekend, so let's look at that code and study the Unit tests.

`let parts = try MultipartParser().parse(data: data, boundary: "----WebKitFormBoundaryPVOZifB9OqEwP2fn")`

Hmm, okay. I have to use the MultipartParser. But how do I know the boundary which is created by the browser? Where is that hidden in the request? I searched at that time, but could not find it. 
Also: this is not the way that decodable is supposed to work, not?

Later I found that boundary is part of the request.content, and I can get it with
`req.content.container.http.headers["content-type"]`
and some regex. However:
`container' is inaccessible due to 'internal' protection level`
So, that is clearly not the way.

## MultipartPart
Oh, I also tried to decode to MultipartPart, but that is not decodable.

## Epilogue
I hope you can see I explored the documentation of Multipart enough?

I think MultipartForm was simpler.
I am sure you can prove me wrong.
