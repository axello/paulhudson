import Foundation
import Routing
import Vapor
import Multipart
import SwiftGD

public func routes(_ router: Router) throws {
    let rootDirectory = DirectoryConfig.detect().workDir
    let uploadDirectory = URL(fileURLWithPath: "\(rootDirectory)Public/uploads")
    let originalsDirectory = uploadDirectory.appendingPathComponent("originals")
    let thumbsDirectory = uploadDirectory.appendingPathComponent("thumbs")

    router.get { req -> Future<View> in
        let fm = FileManager()
        
        guard let files = try? fm.contentsOfDirectory(at: originalsDirectory, includingPropertiesForKeys: nil) else {
            throw Abort(.internalServerError)
        }
        
        let allFilenames = files.map { $0.lastPathComponent }
        let visibleFilenames = allFilenames.filter { !$0.hasPrefix(".") }
        
        let context = ["files": visibleFilenames]
        return try req.view().render("home", context)
    }

    // single ImageFile, multiple files
    router.post("upload2") { req -> Future<Response> in
        struct ImageFile : Content {
            var upload: [File]
        }
        return try req.content.decode(ImageFile.self).map(to: Response.self, { forms in
            let ploep = forms.upload            // for some reason, this is always 0
            print("files uploaded: \(ploep)")
            guard forms.upload.count > 0 else {
                return req.redirect(to: "/", type: RedirectType.normal)
            }
            for file in forms.upload {
                print(file.filename)
                let newURL = originalsDirectory.appendingPathComponent("wtf_\(file.filename).png")
                print(newURL)

                _ = try? file.data.write(to: newURL)
            }
            return req.redirect(to: "/", type: RedirectType.normal)          //.redirect(to: "/", [:])
       })
    }

    router.post("upload") { req -> Future<Response> in
        struct UserFile: Content {
            var upload: [File]
        }
        
        // pull out the multi-part encoded form data
        return try req.content.decode(UserFile.self).map(to: Response.self) { data in
            // create an array of the file types we're willing to accept
            let acceptableTypes = [MediaType.png, MediaType.jpeg]
            let ploep = data.upload            // for some reason, this is always 0
            print("files uploaded: \(ploep)")

            // loop over each file in the uploaded array
            for file in data.upload {
                // ensure this image is one of the valid types
//                let lowercaseType = file.contentType.
                guard let mimeType = file.contentType else { continue }
                guard acceptableTypes.contains(mimeType) else { continue }
                
                // replace any spaces in filenames with a dash
                let cleanedFilename = file.filename.replacingOccurrences(of: " ", with: "-")
                
                // convert that into a URL we can write to
                let newURL = originalsDirectory.appendingPathComponent(cleanedFilename)
                print(newURL)

                // write the full-size original image
                _ = try? file.data.write(to: newURL)
                
                // create a matching URL in the thumbnails directory
                let thumbURL = thumbsDirectory.appendingPathComponent(cleanedFilename)
                
                // attempt to load the original into a SwiftGD image
                if let image = Image(url: newURL) {
                    // attempt to resize that down to a thumbnail
                    if let resized = image.resizedTo(width: 300) {
                        // it worked – save it!
                        resized.write(to: thumbURL)
                    }
                }
            }
            
            return req.redirect(to: "/")
        }
    }
}
