//
//  Join.swift
//  App
//
//  Created by Axel Roest on 25/04/2018.
//

import Foundation
import Async
import Leaf

public final class JoinTag: TagRenderer {
    public func render(tag parsed: TagContext) throws -> Future<TemplateData> {
        try parsed.requireParameterCount(2)
        
        return Future.map(on: parsed.container) {
            if let array = parsed.parameters[0].array, let separator = parsed.parameters[1].string {
                // success
                let items = array.compactMap { $0.string }
                return .string(items.joined(separator: separator))
            } else {
                // failure
                return .null
            }
        }
    }
}
