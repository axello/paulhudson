//
//  Article.swift
//  App
//
//  Created by Axel Roest on 10/04/2018.
//

import Vapor

struct Article: Parameter, Content {
    var id: Int
    var title: String
  
    init(id: Int) {
        self.id = id
        self.title = "Custom parameters rock!"
    }

//    init(id: String) {    // before futures
//        if let intID = Int(id) {
//            self.id = intID
//            self.title = "Custom parameters rock!"
//        } else {
//            self.id = 0
//            self.title = "Unknown article"
//        }
//    }

    // return a future Article, without error checking
    static func resolveParameter(_ parameter: String, on container: Container) throws -> Article {
        if let id = Int(parameter) {
            // conversion worked, return an article
            return Article(id: id)
        } else {
            // conversion failed : throw an error
            throw Abort(.badRequest)
        }
    }
    
    // return an Article, without error checking, before futures
//    static func resolveParameter(_ parameter: String, on container: Container) throws -> Article {
//        return Article(id: parameter)
//    }


}
