import Fluent
import FluentSQLite
import Foundation
import Vapor
import Authentication

struct User: Content, SQLiteModel, Migration {
    var id: Int?
    var username: String
    var password: String
    var language: String
}

extension User: BasicAuthenticatable {
    static let usernameKey = \User.username
    static let passwordKey = \User.password
}
