import Authentication
import Foundation
import FluentSQLite
import Vapor

struct Project: Content, SQLiteModel, Migration {
    var id: Int?
    var owner: Int
    var name: String
    var language: String
    var description: String
    var date: Date
}
