//
//  Token.swift
//  App
//
//  Created by Axel Roest on 26/04/2018.
//

import Foundation
import FluentSQLite
import Vapor

struct Token: Content, SQLiteUUIDModel, Migration {
    var id: UUID?
    var username: String
    var expiry: Date
}
