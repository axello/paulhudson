import Foundation
import Routing
import Vapor
import Fluent
import FluentSQLite
import Crypto

/**
    Mock data
 curl http://localhost:8080/create -d "id=twostraws" -d "password=frosties"
 curl http://localhost:8080/create -d "id=taylor" -d "password=swift"
 curl http://localhost:8080/create -d "id=bieber" -d "password=alien"

 curl http://localhost:8080/login -d "id=twostraws" -d "password=frosties"

 curl http://localhost:8080/post -d "token=YOUR_TOKEN_HERE" -d "message=Hello, Barkr"
 curl http://localhost:8080/post -d "token=4E4BD47E-AAC4-41BC-91BB-F086C8C54A98" -d "message=Hello, Barkr"
 
 succeeding post
 curl http://localhost:8080/post -d "token=4E4BD47E-AAC4-41BC-91BB-F086C8C54A98" -d "message=abc" -d "reply=0"
 failing post
 curl http://localhost:8080/post -d "token=4E4BD47E-AAC4-41BC-91BB-F086C8C54A98" -d "message=a" -d "reply=-1"

 Excerpt From: Paul Hudson. “Server-Side Swift Vapor Edition”. Apple Books.
 */
/// Register your application's routes here.
///
/// [Learn More →](https://docs.vapor.codes/3.0/getting-started/structure/#routesswift)
public func routes(_ router: Router) throws {
    router.get { req -> Future<[Post]> in
        return Post.query(on: req).all()
    }
    
    router.post(User.self, at: "create") { req, user -> Future<User> in
        var user2 = try req.content.syncDecode(User.self)

        return User.find(user.id!, on: req).flatMap(to: User.self) { existing in
            guard existing == nil else {
                throw Abort(.badRequest)
            }
            let pw = try BCrypt.hash(user.password, cost: 4)
            user2.password = pw
            
            return user2.create(on: req).map(to: User.self) { user in
                return user
            }
        }
    }
    
    router.post("login") { req -> Future<Token> in
        // pull out the two fields we need
        let username: String = try req.content.syncGet(at: "id")
        let password: String = try req.content.syncGet(at: "password")
        
        // ensure they have meaningful content
        guard username.count > 0, password.count > 0 else {
            throw Abort(.badRequest)
        }
        
        // find the user that matched the login request
        return User.find(username, on: req).flatMap(to: Token.self) { user in
            // delete any expired tokens
            _ = Token.query(on: req).filter(\.expiry < Date()).delete()
            
            // if there isn't one, bail out
            guard let user = user else {
                throw Abort(.notFound)
            }
            
            // check the password is correct
            guard try BCrypt.verify(password, created: user.password) else {
                throw Abort(.unauthorized)
            }
            
            // generate a new token and send it back
            let newToken = Token(id: nil, username: username, expiry: Date().addingTimeInterval(86400))
            return newToken.create(on: req).map(to: Token.self) { newToken in
                return newToken
            }
        }
    }
    
    router.post("post") { req -> Future<Post> in
        // pull out the two fields we need
        let token: UUID = try req.content.syncGet(at: "token")
        let message: String = try req.content.syncGet(at: "message")
        
        // ensure we have meaningful content
        guard message.count > 0 else {
            throw Abort(.badRequest)
        }
        
        // use the reply ID if we have one, or default to zero
        let reply: Int = (try? req.content.syncGet(at: "reply")) ?? 0
        // find the authentication token we were given
        return Token.find(token, on: req).flatMap(to: Post.self) { token in
            // if we can't find one, bail out
            guard let token = token else {
                throw Abort(.unauthorized)
            }
            
            // create a new post and save it to the database
            let post = Post(id: nil, username: token.username, message: message, parent: reply, date: Date())
            try post.validate()
            return post.create(on: req).map(to: Post.self) { post in
                return post
            }
        }
    }
    
    router.get(String.parameter, "posts") { req -> Future<[Post]> in
        let username = try req.parameters.next(String.self)
        return Post.query(on: req).filter(\Post.username == username).all()
    }
    
    router.get("search") { req -> Future<[Post]> in
        let query = try req.query.get(String.self, at: ["query"])
        return Post.query(on: req).filter(\.message ~~ query).all()
    }
}
